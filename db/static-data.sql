INSERT INTO public.my_user(id, create_date, email, first_name, last_name, password, username)
VALUES (1,now(),'admin@gmail.com','admin','admin','encoded password','admin123');

INSERT INTO public.role (id, activity, create_date, name) VALUES (1, true, now(), 'SUPER_ADMIN');
INSERT INTO public.role (id, activity, create_date, name) VALUES (2, true, now(), 'ADMIN');
INSERT INTO public.role (id, activity, create_date, name) VALUES (3, true, now(), 'MANAGER');
INSERT INTO public.role (id, activity, create_date, name) VALUES (4, true, now(), 'SUPERVISOR');
INSERT INTO public.role (id, activity, create_date, name) VALUES (5, true, now(), 'USER');

INSERT INTO public.permission (id, activity, create_date, name) VALUES (1, true, now(), 'CUSTOMER_READ');
INSERT INTO public.permission (id, activity, create_date, name) VALUES (2, true, now(), 'CUSTOMER_WRITE');
INSERT INTO public.permission (id, activity, create_date, name) VALUES (3, true, now(), 'CUSTOMER_UPDATE');
INSERT INTO public.permission (id, activity, create_date, name) VALUES (4, true, now(), 'CUSTOMER_DELETE');
INSERT INTO public.permission (id, activity, create_date, name) VALUES (5, true, now(), 'EMPLOYEE_READ');
INSERT INTO public.permission (id, activity, create_date, name) VALUES (6, true, now(), 'EMPLOYEE_WRITE');
INSERT INTO public.permission (id, activity, create_date, name) VALUES (7, true, now(), 'EMPLOYEE_UPDATE');
INSERT INTO public.permission (id, activity, create_date, name) VALUES (8, true, now(), 'EMPLOYEE_DELETE');

