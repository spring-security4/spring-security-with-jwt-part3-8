package com.example.springsecurityjwt3.dto.payload.login;

import lombok.Data;

@Data
public class LoginRequest {
    private String username;
    private String password;
}
