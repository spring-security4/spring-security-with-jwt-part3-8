package com.example.springsecurityjwt3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateRolePermissionRequest {

    private Integer roleId;
    private Set<Integer> permissionIds;
}
