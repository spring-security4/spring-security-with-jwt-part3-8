package com.example.springsecurityjwt3.mapper;

import com.example.springsecurityjwt3.dto.payload.register.RegisterRequest;
import com.example.springsecurityjwt3.model.MyUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper
public interface UserRegisterMapper {
    UserRegisterMapper INSTANCE = Mappers.getMapper(UserRegisterMapper.class);
    MyUser toEntity(RegisterRequest registerRequest);

}
