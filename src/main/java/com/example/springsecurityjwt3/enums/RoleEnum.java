package com.example.springsecurityjwt3.enums;

public enum RoleEnum {
    SUPER_ADMIN(1),
    ADMIN(2),
    MANAGER(3),
    SUPERVISOR(4),
    USER(5);

    private int id;

    RoleEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
