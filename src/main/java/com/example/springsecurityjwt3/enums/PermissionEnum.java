package com.example.springsecurityjwt3.enums;

public enum PermissionEnum {
    CUSTOMER_READ(1),
    CUSTOMER_WRITE(2),
    CUSTOMER_UPDATE(3),
    CUSTOMER_DELETE(4),
    EMPLOYEE_READ(5),
    EMPLOYEE_WRITE(6),
    EMPLOYEE_UPDATE(7),
    EMPLOYEE_DELETE(8);

    private int id;

    PermissionEnum(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
