package com.example.springsecurityjwt3.auth;

import com.example.springsecurityjwt3.enums.RoleEnum;
import com.example.springsecurityjwt3.model.Authority;
import com.example.springsecurityjwt3.model.MyUser;
import com.example.springsecurityjwt3.model.staticdata.Permission;
import com.example.springsecurityjwt3.model.staticdata.Role;
import com.example.springsecurityjwt3.repository.AuthorityRepository;
import com.example.springsecurityjwt3.repository.MyUserRepository;
import com.example.springsecurityjwt3.repository.PermissionRepository;
import com.example.springsecurityjwt3.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
public class AuthUserDetailsService implements UserDetailsService {

    private final MyUserRepository myUserRepository;
    private final AuthorityRepository authorityRepository;
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<MyUser> myUser = myUserRepository.findMyUserByUsername(username);
        if (myUser.isPresent()) {
            return new AuthUserDetails(myUser.get(), getAuthority(myUser.get().getId()));
        }
        throw new UsernameNotFoundException("User not found");
    }

    private AuthorityDetails getAuthority(Long id) {
        Authority authority = authorityRepository.findAuthorityByUserId(id);
        return AuthorityDetails.builder()
                .roleName(roleRepository.findById(authority.getRoleId()).get().getName())
                .permissionName(authority.getPermissionId()
                        .stream()
                        .map(permisisionId->{
                            Optional<Permission> permission = permissionRepository.findById(permisisionId);
                            return permission.get();
                        })
                        .collect(Collectors.toSet())
                        .stream()
                        .map(Permission::getName)
                        .collect(Collectors.toList()))
                .build();
    }
}
