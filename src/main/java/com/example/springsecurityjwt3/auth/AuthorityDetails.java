package com.example.springsecurityjwt3.auth;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AuthorityDetails {

    private String roleName;
    private List<String > permissionName;
}
