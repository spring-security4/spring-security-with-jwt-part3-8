package com.example.springsecurityjwt3.resource;

import com.example.springsecurityjwt3.dto.UpdateRolePermissionRequest;
import com.example.springsecurityjwt3.service.AccessRolePermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class UpdateRolePermissionResource {

    private final AccessRolePermissionService accessRolePermissionService;

    @PutMapping("/{userId}")
    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    public ResponseEntity<?> giveAccessForUser(@PathVariable Long userId, @RequestBody UpdateRolePermissionRequest updateRolePermissionRequest){
        return ResponseEntity.ok(accessRolePermissionService.updateRoleAndPermission(userId, updateRolePermissionRequest));
    }

}
