package com.example.springsecurityjwt3.resource;

import lombok.extern.java.Log;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customer")
public class CustomerResource {

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('CUSTOMER_READ')")
    public String getCustomerById(@PathVariable Long id){
        return "CUSTOMER BY ID " + id;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('CUSTOMER_READ')")
    public String getAllCustomer(){
        return "ALL CUSTOMER";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('CUSTOMER_WRITE')")
    public String saveCustomer(){
        return "SAVED CUSTOMER";
    }

    @PutMapping
    @PreAuthorize("hasAuthority('CUSTOMER_UPDATE')")
    public String updateCustomer(){
        return "UPDATED CUSTOMER";
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('CUSTOMER_DELETE')")
    public String deleteCustomer(@PathVariable Long id){
        return "CUSTOMER BY ID " + id + " DELETED";
    }
}
