package com.example.springsecurityjwt3.resource;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")
public class EmployeeResource {

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('EMPLOYEE_READ')")
    public String getEmployeeById(@PathVariable Long id){
        return "EMPLOYEE BY ID " + id;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('EMPLOYEE_READ')")
    public String getAllEmployee(){
        return "ALL EMPLOYEE";
    }

    @PostMapping
    @PreAuthorize("hasAuthority('EMPLOYEE_WRITE')")
    public String saveEmployee(){
        return "SAVED EMPLOYEE";
    }

    @PutMapping
    @PreAuthorize("hasAuthority('EMPLOYEE_UPDATE')")
    public String updateEmployee(){
        return "UPDATED EMPLOYEE";
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('EMPLOYEE_DELETE')")
    public String deleteEmployee(@PathVariable Long id){
        return "EMPLOYEE BY ID " + id + " DELETED";
    }
}
