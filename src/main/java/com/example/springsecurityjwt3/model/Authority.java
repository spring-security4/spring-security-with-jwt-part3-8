package com.example.springsecurityjwt3.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Data
public class Authority {

    /**
     * Bu sefer ki Security kodumuzda table lar arasindaki elaqeni one to many ve ya one to one ile deyil
     * \sadece Authority classinda role id user id ve permission id leri saxliyiriq
     * ve id e gore axtarib tapiriq ve yetin edirkki userin role ve permissionlari nedir
     *  private Set<Integer> permissionId; burda ister set ister List olsun Entity zamani
     *  ElementCollection oldugunu demeniz lazimdir eks teqdirde compile time error verecek
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    private Integer roleId;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Integer> permissionId;

    @CreatedDate
    private LocalDateTime createDate;
}
