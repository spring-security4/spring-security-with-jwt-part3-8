package com.example.springsecurityjwt3.service;

import com.example.springsecurityjwt3.dto.payload.login.LoginRequest;
import com.example.springsecurityjwt3.dto.payload.login.LoginResponse;
import com.example.springsecurityjwt3.dto.payload.register.RegisterRequest;
import com.example.springsecurityjwt3.dto.payload.register.RegisterResponse;

public interface AuthService {

     LoginResponse login(LoginRequest loginRequest);
     RegisterResponse registerUser(RegisterRequest registerRequest);
}
