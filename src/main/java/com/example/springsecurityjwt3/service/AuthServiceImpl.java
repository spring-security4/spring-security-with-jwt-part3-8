package com.example.springsecurityjwt3.service;

import com.example.springsecurityjwt3.dto.payload.login.LoginRequest;
import com.example.springsecurityjwt3.dto.payload.login.LoginResponse;
import com.example.springsecurityjwt3.dto.payload.register.RegisterRequest;
import com.example.springsecurityjwt3.dto.payload.register.RegisterResponse;
import com.example.springsecurityjwt3.jwt.JwtUtils;
import com.example.springsecurityjwt3.mapper.UserRegisterMapper;
import com.example.springsecurityjwt3.model.Authority;
import com.example.springsecurityjwt3.model.MyUser;
import com.example.springsecurityjwt3.repository.AuthorityRepository;
import com.example.springsecurityjwt3.repository.MyUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Set;

import static com.example.springsecurityjwt3.enums.PermissionEnum.CUSTOMER_READ;
import static com.example.springsecurityjwt3.enums.PermissionEnum.CUSTOMER_WRITE;
import static com.example.springsecurityjwt3.enums.RoleEnum.USER;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService{

    private final MyUserRepository myUserRepository;
    private final AuthorityRepository authorityRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;
    private final PasswordEncoder passwordEncoder;

    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtUtils.generateToken(authentication);
        return new LoginResponse(token);
    }

    @Override
    public RegisterResponse registerUser(RegisterRequest registerRequest) {
        MyUser myUser = UserRegisterMapper.INSTANCE.toEntity(registerRequest);
        myUser.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        myUser = myUserRepository.save(myUser);
        createAuthorityForUser(myUser.getId());
        return new RegisterResponse("SUCCESS");
    }

    private void createAuthorityForUser(Long id) {
        Authority authority = new Authority();
        authority.setUserId(id);
        authority.setRoleId(USER.getId());
        authority.setPermissionId(Set.of(CUSTOMER_READ.getId(),CUSTOMER_WRITE.getId()));
        authorityRepository.save(authority);
    }
}
