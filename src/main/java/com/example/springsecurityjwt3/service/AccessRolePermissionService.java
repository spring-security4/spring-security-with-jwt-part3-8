package com.example.springsecurityjwt3.service;

import com.example.springsecurityjwt3.dto.UpdateRolePermissionRequest;

public interface AccessRolePermissionService {

     String updateRoleAndPermission(Long userId, UpdateRolePermissionRequest updateRolePermissionRequest);
}
