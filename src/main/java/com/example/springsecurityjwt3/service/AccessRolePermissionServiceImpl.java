package com.example.springsecurityjwt3.service;

import com.example.springsecurityjwt3.dto.UpdateRolePermissionRequest;
import com.example.springsecurityjwt3.model.Authority;
import com.example.springsecurityjwt3.repository.AuthorityRepository;
import com.example.springsecurityjwt3.repository.MyUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccessRolePermissionServiceImpl implements AccessRolePermissionService {

    private final AuthorityRepository authorityRepository;
    private final MyUserRepository myUserRepository;

    @Override
    public String updateRoleAndPermission(Long userId, UpdateRolePermissionRequest updateRolePermissionRequest) {
        if(myUserRepository.existsMyUserById(userId)){
            Authority authority = authorityRepository.findAuthorityByUserId(userId);
            authority.setRoleId(updateRolePermissionRequest.getRoleId());
            authority.setPermissionId(updateRolePermissionRequest.getPermissionIds());
            authorityRepository.save(authority);
            return "SUCCESS";
        }
        return "ERROR";
    }
}
