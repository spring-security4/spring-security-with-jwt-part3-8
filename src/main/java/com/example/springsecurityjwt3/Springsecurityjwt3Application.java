package com.example.springsecurityjwt3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class Springsecurityjwt3Application {

    public static void main(String[] args) {
        SpringApplication.run(Springsecurityjwt3Application.class, args);
    }

}
