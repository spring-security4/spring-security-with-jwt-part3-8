package com.example.springsecurityjwt3.repository;

import com.example.springsecurityjwt3.model.staticdata.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
}
