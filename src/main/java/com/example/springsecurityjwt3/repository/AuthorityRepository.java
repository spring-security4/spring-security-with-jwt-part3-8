package com.example.springsecurityjwt3.repository;

import com.example.springsecurityjwt3.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority,Long> {

    Authority findAuthorityByUserId(Long userId);
}
