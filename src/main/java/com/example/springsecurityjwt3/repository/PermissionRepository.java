package com.example.springsecurityjwt3.repository;

import com.example.springsecurityjwt3.model.staticdata.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends JpaRepository<Permission,Integer> {
}
